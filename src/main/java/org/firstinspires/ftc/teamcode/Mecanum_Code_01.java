package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

// import static android.os.SystemClock.sleep;

@TeleOp(name="Mecanum: TeleOp-DriveCode", group="Iterative Opmode")
public class Mecanum_Code_01 extends OpMode {
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor topRight_Motor; // intializing top right motor
    private DcMotor topLeft_Motor;  // intializing top left motor
    private DcMotor bottomRight_Motor; // intializing bottom right motor
    // intializing top left motor
    private DcMotor bottomLeft_Motor;

    private double prevTLPower = 0, tlTotal = 0;
    private double prevTRPower = 0, trTotal = 0;
    private double prevBRPower = 0, brTotal = 0;
    private double prevBLPower = 0, blTotal = 0;

    public void init() {
        topRight_Motor = hardwareMap.get(DcMotor.class, "RightFront");  // setting up the top right motor
        topLeft_Motor = hardwareMap.get(DcMotor.class, "LeftFront");    // setting up top left motor
        bottomRight_Motor = hardwareMap.get(DcMotor.class, "RightRear");  // setting bottom right motor
        bottomLeft_Motor = hardwareMap.get(DcMotor.class,"LeftRear");    // setting bottom left motor
    }
    @Override
    public void init_loop() {
    }
    @Override
    public void start() {
        runtime.reset();
        topLeft_Motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        topRight_Motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        bottomLeft_Motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        bottomRight_Motor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    public void loop(){

        //colorSensor.enableLed(false);
        double x = gamepad1.left_stick_x;
        double y = -gamepad1.left_stick_y;
        double rotation = gamepad1.right_stick_x;
        boolean pushA = gamepad2.a;
        double topLeft_Power = (x - y - rotation);
        double topRight_Power = (x + y - rotation);
        double bottomRight_Power = (-x + y - rotation);
        double bottomLeft_Power = (-x - y - rotation);
        double[] motorPower = {topLeft_Power,topRight_Power, bottomLeft_Power, bottomRight_Power};

        double largestPower = findLargest(motorPower);

        topRight_Power *= 0.8;
        topLeft_Power *= 0.8;
        bottomLeft_Power *= 0.8;
        bottomRight_Power *= 0.8;
        if (largestPower > 1) {
            topLeft_Power /= largestPower;
            topRight_Power /= largestPower;
            bottomLeft_Power /= largestPower;
            bottomRight_Power /= largestPower;
        }
        telemetry.addData("TL Power", topLeft_Power);
        telemetry.addData("BL Power", bottomLeft_Power);
        telemetry.addData("TR Power", topRight_Power);
        telemetry.addData("BR Power", bottomRight_Power);
        bottomLeft_Motor.setPower(bottomLeft_Power);
        topLeft_Motor.setPower(topLeft_Power);
        topRight_Motor.setPower(topRight_Power);
        bottomRight_Motor.setPower(bottomRight_Power);
        telemetry.addData("X Position: " , x);
        telemetry.addData("Y Position: " , y);

        


        prevTLPower = topLeft_Power;
        tlTotal = prevTLPower;
        prevTRPower = topRight_Power;
        trTotal = prevTRPower;
        prevBRPower = bottomRight_Power;
        brTotal = prevBRPower;
        prevBLPower = bottomLeft_Power;
        blTotal = prevBLPower;
    }

    private double findLargest(double[] motorPower) {
        double largest = Math.abs(motorPower[0]);
        for(double power : motorPower) {
            if(Math.abs(power) > largest) {
                largest = Math.abs(power);
            }
        }
        return largest;
    }
}

/*
        boolean up = gamepad1.dpad_up;
        boolean right = gamepad1.dpad_right;
        boolean left =  gamepad1.dpad_left;
        boolean down = gamepad1.dpad_down;

        if(up){
            for(int i = 0; i < 1; i += 0.01) {
                topLeft_Motor.setPower(-i);
                topRight_Motor.setPower(i);
                bottomLeft_Motor.setPower(i);
                bottomRight_Motor.setPower(-i);
            }
        }
        else if(down){
            for(int i = 0; i < 1; i += 0.01) {
                topLeft_Motor.setPower(i);
                topRight_Motor.setPower(-i);
                bottomLeft_Motor.setPower(-i);
                bottomRight_Motor.setPower(i);
            }
        }
        else if(left){
            for(int i = 0; i < 1; i += 0.01) {
                topLeft_Motor.setPower(-i);
                topRight_Motor.setPower(-i);
                bottomLeft_Motor.setPower(i);
                bottomRight_Motor.setPower(i);
            }
        }
        else if(right){
            for(int i = 0; i < 1; i += 0.01) {
                topLeft_Motor.setPower(i);
                topRight_Motor.setPower(i);
                bottomLeft_Motor.setPower(-i);
                bottomRight_Motor.setPower(-i);
            }
        }
        else {
                topLeft_Motor.setPower(0);
                topRight_Motor.setPower(0);
                bottomLeft_Motor.setPower(0);
                bottomRight_Motor.setPower(0);
        }
*/
